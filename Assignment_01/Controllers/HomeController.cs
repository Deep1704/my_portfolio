﻿using Assignment_01.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace Assignment_01.Controllers

{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            ViewBag.Title = "Home";
            return View("Index");
        }

        public IActionResult about()
        {
            ViewBag.Title = "about";
            return View();
        }

        public IActionResult resume()
        {
            ViewBag.Title = "resume";
            return View();
        }

        public IActionResult portfolio()
        {
            ViewBag.Title = "portfolio";
            return View();
        }

        public IActionResult services()
        {
            ViewBag.Title = "services";
            return View();
        }

        public IActionResult contact()
        {
            ViewBag.Title = "contact";
            return View();
        }
    }
}